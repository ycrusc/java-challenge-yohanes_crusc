package ist.challange.yohanes_crusc.exception;

import org.springframework.http.*;

import java.util.*;

public class ExistException {
    public static ResponseEntity<Object> existsException( HttpStatus status, Object responseObj) {

        Map<String,Object> map = new HashMap<String,Object>();
        map.put("status", status.value());
        map.put("data", responseObj);

        return new ResponseEntity<Object>(map,status);
    }
}
