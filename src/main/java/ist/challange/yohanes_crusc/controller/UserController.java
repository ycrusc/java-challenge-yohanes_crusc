package ist.challange.yohanes_crusc.controller;

import ist.challange.yohanes_crusc.dto.UserRequest;
import ist.challange.yohanes_crusc.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
public class UserController {

    UserService userService;

    @GetMapping("user/all")
    public ResponseEntity <Object> getAll(){
        return userService.getAll();
    }

    @PostMapping("user/registrasi")
    public ResponseEntity<Object> registrasiUser(@RequestBody UserRequest userRequest){
        return userService.createUser(userRequest);
    }

    @PostMapping("user/login")
    public ResponseEntity<Object> loginUser(@RequestBody UserRequest userRequest){
        return userService.loginUser(userRequest);
    }

    @PutMapping("user/update/{userId}")
    public ResponseEntity<Object> updateUser(@PathVariable Long userId, @RequestBody UserRequest userRequest){
        return userService.updateUser(userId, userRequest);
    }
}
