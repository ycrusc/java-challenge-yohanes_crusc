package ist.challange.yohanes_crusc.service;

import ist.challange.yohanes_crusc.dto.UserRequest;
import org.springframework.http.ResponseEntity;

public interface UserService {
    ResponseEntity<Object> getAll();

    ResponseEntity<Object> createUser(UserRequest userRequest);

    ResponseEntity<Object> loginUser(UserRequest userRequest);

    ResponseEntity<Object> updateUser(Long Id, UserRequest userRequest);

}
