package ist.challange.yohanes_crusc.service.implement;

import ist.challange.yohanes_crusc.dto.*;
import ist.challange.yohanes_crusc.entity.User;
import ist.challange.yohanes_crusc.exception.ExistException;
import ist.challange.yohanes_crusc.exception.ResourceNotFoundException;
import ist.challange.yohanes_crusc.repository.UserRepo;
import ist.challange.yohanes_crusc.exception.ResponseHandler;
import ist.challange.yohanes_crusc.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.http.*;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import java.util.*;

@Service
@AllArgsConstructor
public class UserServiceImplement implements UserService {

    UserRepo userRepo;

    PasswordEncoder passwordEncoder;

    @Override
    public ResponseEntity<Object> getAll() {
        try{
            List<User> user = userRepo.findAll();
            if(user.isEmpty()){
                throw new ResourceNotFoundException("Tidak ada data user.");
            }

            List<UserResponse> userResponsesList = new ArrayList<>();
            for(User dataresult:user){
                Map<String, Object> users = new HashMap<>();
                users.put("User Id : ", dataresult.getId());
                users.put("User name: ", dataresult.getUsername());
                UserResponse userResponse = dataresult.convertToResponse();
                userResponsesList.add(userResponse);
            }
            return ResponseHandler.generateResponse("Sukses Mendapatkan Data", HttpStatus.OK, userResponsesList);
        }catch (Exception e){
            return ResponseHandler.generateResponse("Bad Request", HttpStatus.MULTI_STATUS, "Bad Request!");
        }
    }

    @Override
    public ResponseEntity<Object> createUser(UserRequest userRequest) {
        try{
            if(userRequest.getUsername().isEmpty() || userRequest.getPassword().isEmpty()){
                throw new ResourceNotFoundException("Username dan/atau password kosong");
            }

            if(userRepo.existsByUsername(userRequest.getUsername())){
               return ExistException.existsException(HttpStatus.CONFLICT, "Username sudah terpakai");
            }

            User user = new User();
            user.setUsername(userRequest.getUsername());
            user.setPassword(passwordEncoder.encode(userRequest.getPassword()));
            userRepo.save(user);
            UserResponse userResponse = user.convertToResponse();
            return ResponseHandler.generateResponse("Sukses menyimpan user", HttpStatus.CREATED, userResponse);
        } catch (Exception e){
            return ResponseHandler.generateResponse("Bad Request", HttpStatus.BAD_REQUEST, "Sign Up gagal");
        }
    }

    @Override
    public ResponseEntity<Object> loginUser(UserRequest userRequest) {
        try{
            if(userRequest.getUsername().isEmpty() || userRequest.getPassword().isEmpty()){
                throw new ResourceNotFoundException("Username dan/atau password kosong");
            }

            if(!userRepo.existsByUsername(userRequest.getUsername())){
                return ExistException.existsException(HttpStatus.FORBIDDEN, "Username tidak ada");
            }

            Optional<User> user = userRepo.findByUsername(userRequest.getUsername());
            boolean check = passwordEncoder.matches(userRequest.getPassword(), user.get().getPassword());
            if(!check){
                return ExistException.existsException(HttpStatus.FORBIDDEN, "Password salah");
            }
            return ResponseHandler.succesResponse("Sukses Login", HttpStatus.OK);
        }catch (Exception e){
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.BAD_REQUEST, "Login gagal");
        }
    }

    @Override
    public ResponseEntity<Object> updateUser(Long Id, UserRequest userRequest) {
       try{
           User user = userRepo.getById(Id);
          if(userRepo.existsByUsername(userRequest.getUsername())){
               return ExistException.existsException(HttpStatus.CONFLICT, "Username sudah terpakai");
          }

          if(passwordEncoder.matches(userRequest.getPassword(), user.getPassword())){
               return ExistException.existsException(HttpStatus.BAD_REQUEST, "Password tidak boleh sama dengan password sebelumnya");
          }

           user.setId(Id);
           user.setUsername(userRequest.getUsername());
           user.setPassword(passwordEncoder.encode(userRequest.getPassword()));
           userRepo.save(user);
           UserResponse userResponse = user.convertToResponse();
           return ResponseHandler.generateResponse("Sukses merubah data user", HttpStatus.CREATED,userResponse);
       }catch (Exception e){
           return ResponseHandler.generateResponse("Bad Request", HttpStatus.BAD_REQUEST, "Update gagal");
       }
    }
}
