package ist.challange.yohanes_crusc.entity;

import ist.challange.yohanes_crusc.dto.UserResponse;
import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long Id;
    @Column(name = "password")
    private String password;

    @Column(name = "username", length = 25, unique = true)
    private String username;

    public UserResponse convertToResponse(){
        return UserResponse.builder()
                .userId(this.Id)
                .username(this.username)
                .build();
    };

    @Override
    public String toString() {
        return "User{" +
                "Id=" + Id +
                ", password='" + password + '\'' +
                ", username='" + username + '\'' +
                '}';
    }
}
