package ist.challange.yohanes_crusc.dto;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserResponse {

    private Long userId;

    private String username;

    @Override
    public String toString() {
        return "ResponseUser{" +
                "userId=" + userId +
                ", username='" + username + '\'' +
                '}';
    }
}
