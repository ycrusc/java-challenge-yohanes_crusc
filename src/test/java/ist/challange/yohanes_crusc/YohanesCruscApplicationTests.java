package ist.challange.yohanes_crusc;

import ist.challange.yohanes_crusc.dto.*;
import ist.challange.yohanes_crusc.entity.User;
import ist.challange.yohanes_crusc.exception.ExistException;
import ist.challange.yohanes_crusc.exception.ResponseHandler;
import ist.challange.yohanes_crusc.repository.UserRepo;
import ist.challange.yohanes_crusc.service.implement.UserServiceImplement;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.*;
import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
class YohanesCruscApplicationTests {

	@Mock
	private UserRepo userRepo;

	@InjectMocks
	private UserServiceImplement userService;

	private User user;

	@BeforeEach
	public void setup(){

		user = User.builder()
				.Id(1L)
				.username("aku")
				.password("123456")
				.build();
	}

	@DisplayName("JUnit test for getAll User Positive Case")
	@Test
	public void getAllUserPositiveTest(){
		User user1 = User.builder()
				.Id(1L)
				.username("saya")
				.password("123456")
				.build();
		List<UserResponse> userResponsesList = new ArrayList<>();
		user.setUsername(user1.getUsername());
		user.setPassword(user1.getPassword());
		UserResponse userResponse = user.convertToResponse();
		userResponsesList.add(userResponse);
		given(userRepo.findAll()).willReturn(List.of(user));
		ResponseEntity<Object> userList = userService.getAll();

		assertThat(userList).isNotNull();
		assertThat(userResponsesList.size()).isEqualTo(1);
	}

	@DisplayName("JUnit test for getAll User Negative Case(No value)")
	@Test
	public void getAllUserNegativeTest(){
		given(userRepo.findAll()).willReturn(Collections.emptyList());
		ResponseEntity<Object> userList = userService.getAll();

		assertThat(userList).isEqualTo(ResponseHandler.generateResponse("Bad Request", HttpStatus.MULTI_STATUS, "Bad Request!"));
	}

	@DisplayName("JUnit test for updateUser  Negative Case(password null)")
	@Test
	public void updateUserNegativeTest(){
		UserRequest userRequest1 = new UserRequest();
		userRequest1.setPassword(user.getPassword());

		ResponseEntity<Object> updateUser = userService.updateUser(1L, userRequest1);

		assertThat(updateUser).isEqualTo(ResponseHandler.generateResponse("Bad Request", HttpStatus.BAD_REQUEST, "Update gagal"));
	}

	@DisplayName("JUnit test for login user  Negative Case(username not found)")
	@Test
	public void loginUserTest(){
		UserRequest users = new UserRequest();
		users.setUsername("aria");
		users.setPassword("1234");

		ResponseEntity<Object> login = userService.loginUser(users);

		assertThat(login).isEqualTo(ExistException.existsException(HttpStatus.FORBIDDEN, "Username tidak ada"));
	}

	@DisplayName("JUnit test for saveEmployee Positive Case")
	@Test
	public void createUserPositiveTest(){
		UserRequest users = new UserRequest();
		users.setUsername("aku");
		users.setPassword("1234");

		ResponseEntity<Object> savedUser = userService.createUser(users);

		assertThat(savedUser).isNotNull();
	}

	@DisplayName("JUnit test for saveEmployee  Negative Case (username null)")
	@Test
	public void createUserNegativeTest(){
		UserRequest users = new UserRequest();
		users.setPassword("1234");

		ResponseEntity<Object> savedUser = userService.createUser(users);

		assertThat(savedUser).isEqualTo(ResponseHandler.generateResponse("Bad Request", HttpStatus.BAD_REQUEST, "Sign Up gagal"));
	}
}
