CREATE OR REPLACE FUNCTION trigger_set_timestamp()
RETURNS TRIGGER AS $$
BEGIN
  NEW.updated_at = NOW();
  RETURN NEW;
END;
$$ LANGUAGE plpgsql;

create table users (
  	id serial NOT NULL,
	"password" varchar(25) not NULL,
	username varchar(25) not null unique,
   	inserted_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
   	updated_at  TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
   	constraint pk_users primary key (id)
)   ;
CREATE TRIGGER set_timestamp
BEFORE UPDATE ON users
FOR EACH ROW
EXECUTE PROCEDURE trigger_set_timestamp();